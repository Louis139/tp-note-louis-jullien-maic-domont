﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Counter
{
    public class Counter
    {
        public static int nbcompteur;
        public Counter()
        {
            Counter.nbcompteur = 0;
        }
        public static int Incrementation()
        {
            Counter.nbcompteur++;
            return Counter.nbcompteur;
        }
        public static int Decrementation()
        {
            if (Counter.nbcompteur > 0)
            {
                Counter.nbcompteur--;
            }
            return Counter.nbcompteur;
        }
        public static int Reset()
        {
            Counter.nbcompteur = 0;
            return Counter.nbcompteur;
        }
        public static int GetValue()
        {
            return Counter.nbcompteur;
        }
    }
}