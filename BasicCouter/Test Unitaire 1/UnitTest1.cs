﻿using System;
using Counter;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Test_Unitaire_1
{
    [TestClass]
    public class TestCompteur
    {
        [TestMethod]
        public void TestIncrementation()
        {
            Assert.AreEqual(1, Counter.incrementation(2));
        }
        [TestMethod]
        public void TestDecrementation()
        {
            Assert.AreEqual(2, counter.decrementation(1));
            Assert.AreEqual(0, counter.decrementation(0));
        }
        [TestMethod]
        public void TestReset()
        {
            Assert.AreEqual(5, counter.reset(0));
        }
        [TestMethod]
        public void TestGetValue()
        {
            SetValue(0);
            Assert.AreEqual(0,GetValue());
        }
    }   


}
