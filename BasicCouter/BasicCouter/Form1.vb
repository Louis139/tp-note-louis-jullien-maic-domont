﻿Public Class Form1

    Private Sub Button_plus_MouseClick(sender As Object, e As MouseEventArgs) Handles Button_plus.MouseClick
        Label2.Text = CStr(Counter.Counter.incrementation())
    End Sub

    Private Sub Button_moins_MouseClick(sender As Object, e As MouseEventArgs) Handles Button_moins.MouseClick
        Label2.Text = CStr(Counter.Counter.decrementation())
    End Sub

    Private Sub Button_reset_MouseClick(sender As Object, e As MouseEventArgs) Handles Button_reset.MouseClick
        Label2.Text = CStr(Counter.Counter.reset())
    End Sub

End Class
