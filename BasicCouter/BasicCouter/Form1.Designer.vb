﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Button_moins = New System.Windows.Forms.Button()
        Me.Button_plus = New System.Windows.Forms.Button()
        Me.Button_reset = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Button_moins
        '
        Me.Button_moins.AccessibleName = ""
        Me.Button_moins.Location = New System.Drawing.Point(173, 184)
        Me.Button_moins.Name = "Button_moins"
        Me.Button_moins.Size = New System.Drawing.Size(75, 23)
        Me.Button_moins.TabIndex = 0
        Me.Button_moins.Text = "-"
        Me.Button_moins.UseVisualStyleBackColor = True
        '
        'Button_plus
        '
        Me.Button_plus.Location = New System.Drawing.Point(524, 184)
        Me.Button_plus.Name = "Button_plus"
        Me.Button_plus.Size = New System.Drawing.Size(75, 23)
        Me.Button_plus.TabIndex = 1
        Me.Button_plus.Text = "+"
        Me.Button_plus.UseVisualStyleBackColor = True
        '
        'Button_reset
        '
        Me.Button_reset.Location = New System.Drawing.Point(363, 256)
        Me.Button_reset.Name = "Button_reset"
        Me.Button_reset.Size = New System.Drawing.Size(75, 23)
        Me.Button_reset.TabIndex = 2
        Me.Button_reset.Text = "Reset"
        Me.Button_reset.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(384, 126)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(31, 13)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Total"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(375, 159)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(51, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "GetValue"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Button_reset)
        Me.Controls.Add(Me.Button_plus)
        Me.Controls.Add(Me.Button_moins)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Button_moins As Button
    Friend WithEvents Button_plus As Button
    Friend WithEvents Button_reset As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
End Class
